from flask import Flask
from flask import request
import json


certificates= [] 

app = Flask(__name__)



# Endpoint for health check purposes 
@app.route('/')
def hello_world():
    return 'Hi Endpoint is Alive!'


# @app.route('/certificate/<int:id_certicate>')
# def crudCertificate(id_certicate):
#     return 'Id is %d' %id_certicate 



#Creates a new certificate, receives a json with the data containing the certificates
@app.route('/certificates/create', methods=['POST'] )
def createCertificate():
    print (request.is_json)
    certificate = request.get_json()

    if(certificate=={}):
    	return json.dumps({'success': False , "Message": "Empty data json"}), 400, {'ContentType':'application/json'} 	

    if(checkIFCerticateExists(certificate)):
    	return json.dumps({'success': False , "Message": "Certificate with id used already"}), 400, {'ContentType':'application/json'} 	



    certificates.append(certificate)
    print ("certificates=======")
    print(certificates)
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'} 



#Updates a existing certificate, receives a json containing the certificate with the data updated
@app.route('/certificates/update/<string:id_certicate>', methods=['POST'] )
def updateCertificate(id_certicate):
    print (request.is_json)
    newCertificate = request.get_json()

    if(removeCertificate(id_certicate)==False):
        	return json.dumps({'success': False , "Message": "Certificate does not exist"}), 400, {'ContentType':'application/json'} 	



    certificates.append(newCertificate)
    print ("certificates=======")
    print(certificates)


    return json.dumps({'success':True}), 200, {'ContentType':'application/json'} 


#Deletes a certificate with the given id 
@app.route('/certificates/delete/<string:id_certicate>', methods=['GET'] )
def deleteCertificate(id_certicate):
    

    if(removeCertificate(id_certicate)==False):
        	return json.dumps({'success': False , "Message": "Certificate does not exist"}), 400, {'ContentType':'application/json'} 	

    print ("certificates=======")
    print(certificates)    	

    return json.dumps({'success':True}), 200, {'ContentType':'application/json'} 


#Lists the certificates owned by the user
@app.route('/users/<string:user_Id>/certificates')
def getCertificatesofUser(user_Id):
	certificatesUser=[]
	print ("Given user id " + user_Id)
	
	for certificate in certificates:
		print ("Certificate owner id:" +  certificate["ownerId"])
		if(certificate["ownerId"]==user_Id):
			certificatesUser.append(certificate)

	print(certificatesUser)		
	return json.dumps(certificatesUser), 200, {'ContentType':'application/json'}		


#Create a transfer with the json object passed on the request
@app.route('/certificates/<string:certificate_Id>/transfers/create', methods= ['POST'])
def createTransfer(certificate_Id):
	
	transfer=request.get_json()

	print ("Transfer:" + str(transfer))

	if(transfer == None ):
		return json.dumps({ "Status" : "Transfer json object body empty" }), 400, {'ContentType':'application/json'}


	print ("Given id " + certificate_Id)

	print ("Transfer: " + str(transfer))
	

	certificate=getCertificate(certificate_Id)

	if (certificate == None ): 
		 return json.dumps({ "Status" : "error certificate with given id not found"   }), 400, {'ContentType':'application/json'}


	certificate["transfer"]=transfer

	print ("certificates=======")
	print(certificates)   

	return json.dumps({ "Status" : "success"   }), 200, {'ContentType':'application/json'}


#Changes the status of the given transfer from created to completed
@app.route('/certificates/<string:certificate_Id>/transfers/accept', methods= ['GET'])
def acceptTransfer(certificate_Id):
	
	
	print ("Given id " + certificate_Id)

	
	certificate=getCertificate(certificate_Id)

	if (certificate == None): 
		 return json.dumps({ "Status" : "error certificate with given id not found"   }), 400, {'ContentType':'application/json'}


	certificate["transfer"]["status"]="completed"

	print ("certificates=======")
	print(certificates)   

	return json.dumps({ "Status" : "success"   }), 200, {'ContentType':'application/json'}



#Checks if the certificate already exists on the registries
def checkIFCerticateExists(newCertificate):


	for certificate in certificates:
		if certificate["id"] == newCertificate["id"]:
			return True

	
	return False		

#Removes a given certificate
def removeCertificate(id_certicate):

	print ("Id given: " + str(id_certicate)) 

	for certificate in certificates:
		print ("Certificate id: " + str(certificate["id"]))
		
		if (certificate["id"] == id_certicate):
			certificates.remove(certificate)
			return True
	
	return False


#Returns a certificate with the given id
def getCertificate(certificate_Id): 

	for certificate in certificates: 
		if (certificate["id"]==certificate_Id):
			return certificate

	return None		
