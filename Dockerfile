from python:3.7-alpine3.8

ADD * code/
WORKDIR  /code
RUN pip install -r requirements.txt
ENV FLASK_APP=server.py 
CMD flask run --host=0.0.0.0
