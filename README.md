---

---

# Running Test Units

The unit tests can be ran using the pytest command, using the following command: 

```bash
pytest -v
```

# Running the Server

In order to run the server two approaches can be used: 

**Command Line:** 

Using the command line run the following commands: 

```bash
pip install -r requirements.txt
FLASK_APP=server.py 
flask run --host=0.0.0.0
```

It should launch a server listening on port 5000. 

 

**Docker:** 

There is a Dockerfile on the root of the project, in order to run using docker use the following commands:

```bash
docker build . -t flaskserver
docker run -p 5000:5000 flaskserver
```



# Endpoints Documentation

In order to facilitate the use of the endpoints a postman collection is available on the root of the git project, it clarifies the use, utilizing sample requests. 

The use of each endpoint created is documented below, it follows the type of method used (GET or POST) and the json data format expected:  



## **/**  (Method=GET)

Root endpoint used to health checks purposes, returns a string.



## /certificates/create (methods=['POST']) 

Receives a certificate json with the following format:

```json
{
id: 'string'
title: 'string'
createdAt: 'date',
ownerId: 'string',
year: 'number',
note: 'string',
transfer: <object representing the transfer state>
}
```

Observation: The transfer object is passed as a empty json when the certificate is created ( transfer : {} on the JSON above)

It returns a json with a succes message, in case everything works fine, with a code 200, a 400 code is returned otherwise. 

## '/certificates/update/:id_certificate>' (methods=['POST'])

Receives the certificate id on the url of the request, and a certificate JSON passed as data, following the same format as the given above on the /certificates/create endpoint. 

It returns a message of success if everything run as should, but it will return error in case of a wrong id passed or a empty certificate as data. 



## /certificates/delete/:id_certicate (methods=['GET'])

This is a get method to delete a existing certificate, in case of success it will return a JSON with a code 200, but if the id is invalid the request will return a 400 with the cause of the failure. 



## /users/:user_Id>/certificates (methods=['GET'])

Receives the user id on the address of the endpoint, returning a user certificates JSON with the following format:

```JSON
{certificateUser1, certificateUser2, certificateUser3}
```

An empty JSON will be returned if no entries are found.



## /certificates/:certificate_Id>/transfers/create (methods=['POST'])

Receives the certificate id on the endpoint URL and create a transfer for the given certificate. It will return a error if the id is not valid. 

It receives a JSON containing the transfer, having the following format: 

```json
{
to: 'email',
status: 'string',
}
```

It creates a transfer with the status created, changed to completed when the transfer is accepted. 



## /certificates/:certificate_Id/transfers/accept (methods=['GET'])

This get request receives the certificate id to accept the transfer and changes the status of the transfer from created to accepted. It returns a error if id given is not valid.

