import os
import tempfile

import pytest

from server import *

import os
import unittest
 
 
 
TEST_DB = 'test.db'
 
 
class BasicTests(unittest.TestCase):
 
    ############################
    #### setup and teardown ####
    ############################
 
    # executed prior to each test
    def setUp(self):
        app.config['TESTING'] = True
        app.config['DEBUG'] = False
        self.app = app.test_client()
        
 
        # Disable sending emails during unit testing
        
    # executed after each test
    def tearDown(self):
        pass
 
 
###############
#### tests ####
###############
    
    #Test main page
    def test_main_page(self):
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
 
    #Tests valid request to create a new certificate    
    def test_create_certificate(self): 
        
        # certificate=dict(id="0001", title="testCertificate",createdAt=20181020,
        #     ownerId="1020",year=2018,note="lorem ipsum",transfer=dict())

        certificate= {
           "id": "0001",
           "title": "testCertificate",
           "createdAt": 20181020 ,
           "ownerId" : "1020",
           "year" : 2018,
           "note" : "lorem ipsum",
           "transfer" : {}
           } 

        response=self.app.post('/certificates/create', data=json.dumps(certificate),content_type='application/json')
        self.assertEqual(response.status_code, 200)

    #Test the certificate creation with a empty json, should returns a error    
    def test_create_certificate_no_data(self):
        certificate=dict()

        response=self.app.post('/certificates/create', data=json.dumps(certificate),content_type='application/json')
        self.assertEqual(response.status_code, 400)

    
    #Test if the service returns error on a request trying to create a certificate with a id already taken
    def test_create_certificate_same_id(self):
          
        certificate= {
           "id": "0002",
           "title": "testCertificate",
           "createdAt": 20181020 ,
           "ownerId" : "1020",
           "year" : 2018,
           "note" : "lorem ipsum",
           "transfer" : {}
           }

        response=self.app.post('/certificates/create', data=json.dumps(certificate),content_type='application/json')
        print("response: " + str(response))
        self.assertEqual(response.status_code, 200)

        response=self.app.post('/certificates/create', data=json.dumps(certificate),content_type='application/json')
        self.assertEqual(response.status_code, 400)

    #Sends a valid request to update certificate
    def test_update_certificate_right(self):
        certificate= {
           "id": "0002",
           "title": "cahngeTitle",
           "createdAt": 20181020 ,
           "ownerId" : "1020",
           "year" : 2020,
           "note" : "lorem ipsum",
           "transfer" : {}
           }

        response=self.app.post('/certificates/update/0002', data=json.dumps(certificate),content_type='application/json')
        self.assertEqual(response.status_code, 200)

    #Send a invalid request with a id not in use
    def test_update_certificate_wrong_id(self):
        certificate= {
           "id": "0005",
           "title": "cahngeTitle",
           "createdAt": 20181020 ,
           "ownerId" : "1020",
           "year" : 2020,
           "note" : "lorem ipsum",
           "transfer" : {}
           }

        response=self.app.post('/certificates/update/0005', data=json.dumps(certificate),content_type='application/json')
        self.assertEqual(response.status_code, 400)    

    #Sends a valid request to delete a certificate 
    def test_delete_certificate_right_id(self):
        

        response=self.app.get('/certificates/delete/0001')
        self.assertEqual(response.status_code, 200)    

    #Sends invalid request containing a invalid id to delete a certificate
    def test_delete_certificate_wrong_id(self):
        

        response=self.app.get('/certificates/delete/0006')
        self.assertEqual(response.status_code, 400)        

    
    #Valid request to list certificates of a user
    def test_list_user_certificates(self): 
        
        certificate1= {
           "id": "0009",
           "title": "testCertificate",
           "createdAt": 20181020 ,
           "ownerId" : "1030",
           "year" : 2018,
           "note" : "lorem ipsum",
           "transfer" : {}
           }

        certificate2= {
           "id": "0010",
           "title": "testCertificate",
           "createdAt": 20181020 ,
           "ownerId" : "1030",
           "year" : 2018,
           "note" : "lorem ipsum",
           "transfer" : {}
           }

        certificate3= {
           "id": "0011",
           "title": "testCertificate",
           "createdAt": 20181020 ,
           "ownerId" : "1030",
           "year" : 2018,
           "note" : "lorem ipsum",
           "transfer" : {}
           }        

        certificatesUser=[certificate1,certificate2,certificate3]   
           
        response=self.app.post('/certificates/create', data=json.dumps(certificate1),content_type='application/json')
        self.assertEqual(response.status_code, 200)

        response=self.app.post('/certificates/create', data=json.dumps(certificate2),content_type='application/json')
        self.assertEqual(response.status_code, 200)

        response=self.app.post('/certificates/create', data=json.dumps(certificate3),content_type='application/json')
        self.assertEqual(response.status_code, 200)

        response=self.app.get('/users/1030/certificates')
        self.assertEqual(response.status_code, 200)
        
        certificatesResponse = json.loads(response.get_data(as_text=True))

        self.assertEqual(certificatesResponse,certificatesUser)
     
    #List certificates of a user with no certificates issued
    def test_list_user_certificates_empty_response(self):

        response=self.app.get('/users/1090/certificates')
        self.assertEqual(response.status_code, 200)
       
        certificatesResponse = json.loads(response.get_data(as_text=True))

        self.assertEqual(certificatesResponse,[])
   
    #Sends a valid request to create a transfer
    def test_create_transfer(self):
        

        certificate= {
           "id": "0015",
           "title": "testCertificate",
           "createdAt": 20181020 ,
           "ownerId" : "1020",
           "year" : 2018,
           "note" : "lorem ipsum",
           "transfer" : {}
           } 
        
        response=self.app.post('/certificates/create', data=json.dumps(certificate),content_type='application/json')
        self.assertEqual(response.status_code, 200)

        transfer = { "to" : "email" ,
                      "status" : "created"   
        }

        print ("Response create certificate" + str(response.get_json()))

        response = self.app.post('/certificates/0015/transfers/create',data=json.dumps(transfer),content_type='application/json')
        self.assertEqual(response.status_code, 200)
        
    #Sends a request to create transfer with wrong id
    def test_create_transfer_wrong_id(self):
            certificate= {
               "id": "0016",
               "title": "testCertificate",
               "createdAt": 20181020 ,
               "ownerId" : "1020",
               "year" : 2018,
               "note" : "lorem ipsum",
               "transfer" : {}
               } 
            
            response=self.app.post('/certificates/create', data=json.dumps(certificate),content_type='application/json')
            self.assertEqual(response.status_code, 200)

            transfer = { "to" : "email" ,
                          "status" : "created"   
            }

            response = self.app.post('/certificates/0018/transfers/create')
            self.assertEqual(response.status_code, 400)

    #Test if service returns error when a request to create a transfer is made using a empty json data
    def test_create_transfer_empty_data(self):
        certificate= {
           "id": "0019",
           "title": "testCertificate",
           "createdAt": 20181020 ,
           "ownerId" : "1020",
           "year" : 2018,
           "note" : "lorem ipsum",
           "transfer" : {}
           } 
        
        response=self.app.post('/certificates/create', data=json.dumps(certificate),content_type='application/json')
        self.assertEqual(response.status_code, 200)

        transfer = {}

        response = self.app.post('/certificates/0019/transfers/create')
        self.assertEqual(response.status_code, 400)        

    #Sends valid request to accept a transfer
    def test_accept_transfer(self):

        certificate= {
               "id": "0020",
               "title": "testCertificate",
               "createdAt": 20181020 ,
               "ownerId" : "1020",
               "year" : 2018,
               "note" : "lorem ipsum",
               "transfer" : {}
               } 
            
        response=self.app.post('/certificates/create', data=json.dumps(certificate),content_type='application/json')
        self.assertEqual(response.status_code, 200)

        transfer = { "to" : "email" ,
                          "status" : "created"   
            }

        print ("Response create certificate" + str(response.get_json()))

        response = self.app.post('/certificates/0020/transfers/create',data=json.dumps(transfer),content_type='application/json')
        self.assertEqual(response.status_code, 200) 

        response=self.app.get('/certificates/0020/transfers/accept')
        self.assertEqual(response.status_code, 200) 

    #Tries to accept a transfer using a id not in use, tests if a error is returned
    def test_accept_transfer_wrong_id(self):
        response=self.app.get('/certificates/0021/transfers/accept')
        self.assertEqual(response.status_code, 400) 
            



if __name__ == "__main__":
    unittest.main()